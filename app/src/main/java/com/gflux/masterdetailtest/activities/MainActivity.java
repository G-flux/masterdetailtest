package com.gflux.masterdetailtest.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.gflux.masterdetailtest.R;
import com.gflux.masterdetailtest.fragments.DetailFragment;
import com.gflux.masterdetailtest.fragments.MasterFragment;

/**
 * @author Kenneth Saey (G-flux.com)
 * @since 20-05-2015 15:39
 */
public class MainActivity extends FragmentActivity implements MasterFragment.OnMasterSelectedListener {

    public static final String KEY_ITEM = "MainActivity.Item";
    public static final String KEY_POSITION = "MainActivity.Position";

    private boolean isTwoPaneLayout;
    private String item = null;
    private int position = -1;
    private boolean restoreDetailFragment = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View detailView = findViewById(R.id.fragment_detail);
        isTwoPaneLayout = detailView != null && detailView.getVisibility() == View.VISIBLE;
        restoreDetailFragment = false;

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_master, MasterFragment.getInstance())
                    .commit();
            if (isTwoPaneLayout) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_detail, DetailFragment.getInstance())
                        .commit();
            }
        } else {
            item = savedInstanceState.getString(KEY_ITEM);
            position = savedInstanceState.getInt(KEY_POSITION);
            if (isTwoPaneLayout) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_master, MasterFragment.getInstance())
                        .commit();
                restoreDetailFragment = true;
            } else {
                DetailFragment newDetailFragment = DetailFragment.getInstance(item, position);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_master, newDetailFragment)
                        .addToBackStack(null)
                        .commit();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (restoreDetailFragment) {
            DetailFragment detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_detail);
            if (detailFragment != null) {
                detailFragment.updateView(item, position);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_ITEM, item);
        outState.putInt(KEY_POSITION, position);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (isTwoPaneLayout) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onMasterItemSelected(String item, int position) {
        this.item = item;
        this.position = position;
        if (isTwoPaneLayout) {
            DetailFragment detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_detail);
            detailFragment.updateView(item, position);
        } else {
            DetailFragment newDetailFragment = DetailFragment.getInstance(item, position);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_master, newDetailFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }
}
