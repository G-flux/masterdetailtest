package com.gflux.masterdetailtest.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gflux.masterdetailtest.R;

/**
 * @author Kenneth Saey (G-flux.com)
 * @since 20-05-2015 16:12
 */
public class DetailFragment extends Fragment {

    public static final String ARGUMENT_ITEM = "DetailFragment.item";
    public static final String ARGUMENT_POSITION = "DetailFragment.position";

    private String item;
    private int position;

    private TextView title;

    public DetailFragment() {
    }

    public static DetailFragment getInstance() {
        return new DetailFragment();
    }

    public static DetailFragment getInstance(String item, int position) {
        DetailFragment detailFragment = new DetailFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_ITEM, item);
        arguments.putInt(ARGUMENT_POSITION, position);
        detailFragment.setArguments(arguments);
        return detailFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadArguments();
    }

    private void loadArguments() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            if (arguments.containsKey(ARGUMENT_ITEM)) {
                item = arguments.getString(ARGUMENT_ITEM);
            }
            if (arguments.containsKey(ARGUMENT_POSITION)) {
                position = arguments.getInt(ARGUMENT_POSITION);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        title = (TextView) view.findViewById(R.id.title);
        fillView();
        return view;
    }

    private void fillView() {
        if (item != null) {
            title.setText(item);
        }
    }

    public void updateView(String item, int position) {
        this.item = item;
        this.position = position;
        fillView();
    }
}
