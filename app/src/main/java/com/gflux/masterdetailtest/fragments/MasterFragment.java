package com.gflux.masterdetailtest.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gflux.masterdetailtest.R;

/**
 * @author Kenneth Saey (G-flux.com)
 * @since 20-05-2015 15:55
 */
public class MasterFragment extends ListFragment {

    private static final String[] listItems = {"Apple", "Banana", "Cherry", "Durian", "Eggplant", "Figs", "Huckleberries", "Jackfruit", "Kiwi", "Lemon", "Mango", "Nectarine", "Orange", "Passionfruit", "Quince", "Raspberry", "Strawberry", "Tamarillo", "Ugli fruit", "Vidalia Onion", "Watermelon", "Ximena", "Yellow Paprika", "Zucchini"};
    private OnMasterSelectedListener listener;

    public MasterFragment() {
    }

    public static MasterFragment getInstance() {
        return new MasterFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnMasterSelectedListener) {
            listener = (OnMasterSelectedListener) activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_master, container, false);
        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, listItems));
        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        listener.onMasterItemSelected(listItems[position], position);
    }

    public interface OnMasterSelectedListener {
        void onMasterItemSelected(String item, int position);
    }
}
