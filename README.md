MasterDetailTest
================

A robust Master-Detail layout example for Android.

A two pane layout is visible in landscape mode on tablets and a single pane layout in all other cases.

Development
-----------

Author: [@KennethSaey](https://twitter.com/KennethSaey)

Company: [G-flux](http://g-flux.com)

Editor: Developed in Android Studio 1.2.1.1